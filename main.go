package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/snarksliveshere/wire_test/cmd/grpc"
	"gitlab.com/snarksliveshere/wire_test/internal/bootstrap"
	"gitlab.com/snarksliveshere/wire_test/internal/configs"
)

func main() {
	ctx := context.Background()
	var conf configs.AppConfig
	err := envconfig.Process("grpc_server", &conf)
	if err != nil {
		log.Fatalf("%s, %s", "failed to init config", err)
	}

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGINT, syscall.SIGTERM)
	service, err := bootstrap.InitializeSyslog(ctx)
	if err != nil {
		log.Fatalf("%s, %s", "failed to init config", err)
	}
	go func() { grpc.Server(conf, service) }()
	<-interrupt
}
