//+build wireinject

package bootstrap

import (
	"context"

	"github.com/google/wire"

	"gitlab.com/snarksliveshere/wire_test/cmd/grpc"
	"gitlab.com/snarksliveshere/wire_test/internal/configs"
	"gitlab.com/snarksliveshere/wire_test/internal/service/controller"
)

var dbSet = wire.NewSet(
	configs.GetDBConfig,
	newDb,
)

var fileStore = wire.NewSet(
	configs.GetFileConfig,
	newFileStore,
)

func InitializeSyslog(context.Context) (*grpc.ServiceGRPC, error) {
	wire.Build(
		dbSet,
		fileStore,
		loggerInit,
		controller.NewHandleController,
		grpc.NewGRPCService,
	)

	return &grpc.ServiceGRPC{}, nil
}
