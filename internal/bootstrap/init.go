package bootstrap

import (
	"context"
	"log"
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/snarksliveshere/wire_test/internal/configs"
	"gitlab.com/snarksliveshere/wire_test/internal/store/db"
	"gitlab.com/snarksliveshere/wire_test/internal/store/file"
)

var dbAdapterOnce = sync.Once{}
var dbAdapter db.Badger

func newDb(ctx context.Context, cfg configs.DBConfig) (db.SyslogDB, error) {
	var err error
	dbAdapterOnce.Do(func() {
		dbAdapter, err = db.InitDB(ctx, cfg)
	})
	return dbAdapter, err
}

func newFileStore(ctx context.Context, cfg configs.FileConfig) (file.SyslogFile, error) {
	return file.InitFileStore(ctx, cfg)
}

func loggerInit() *zap.SugaredLogger {
	cfg := zap.NewDevelopmentConfig()
	cfg.EncoderConfig.EncodeLevel = customLevelEncoder
	logger, err := cfg.Build()
	if err != nil {
		log.Fatal(err.Error())
	}
	slog := logger.Sugar()
	defer func() { _ = slog.Sync() }()
	slog.Info("GRPC Server Starts")
	return slog
}

func customLevelEncoder(level zapcore.Level, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString("[" + level.CapitalString() + "]")
}
