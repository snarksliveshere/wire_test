package helpers

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetFormatTime(t *testing.T) {
	t.Run("TestGetFormatTimeSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out int64
		}{
			{
				in:  "02.01.2006 15:04:05 UTC",
				out: 1136214245,
			},
			{
				in:  "22.03.2015 18:07:45 UTC",
				out: 1427047665,
			},
			{
				in:  "10.11.2020 13:00:00 UTC",
				out: 1605013200,
			},
		}
		for _, v := range casesSuccess {
			t.Run("if string contains unix time  no err", func(t *testing.T) {
				res, err := GetFormatTime(v.in)
				require.NoError(t, err)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestGetFormatTimeUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out int64
		}{
			{
				in:  "02.01.2006 15:04:05 UTC",
				out: 1036214245,
			},
			{
				in:  "22.03.2015 18:07:45 UTC",
				out: 1407047665,
			},
			{
				in:  "10.11.2020 13:00:00 UTC",
				out: 1605012200,
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if string contains unix time  no equal no err", func(t *testing.T) {
				res, err := GetFormatTime(v.in)
				require.NoError(t, err)
				require.NotEqual(t, v.out, res)
			})
		}
	})

	t.Run("TestGetFormatTimeError", func(t *testing.T) {
		casesError := []struct {
			in string
		}{
			{
				in: "02 01 2006 15 04 05 UTC",
			},
			{
				in: "22 03 2015 18-07-45 UTC",
			},
			{
				in: "10 11 20 13:00:00",
			},
		}
		for _, v := range casesError {
			t.Run("if string contains unix time  err", func(t *testing.T) {
				_, err := GetFormatTime(v.in)
				require.Error(t, err)
			})
		}
	})
}

func TestGetOptimizePeriodTime(t *testing.T) {
	t.Run("TestGetOptimizePeriodTimeSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in, in2   int64
			out, out2 string
		}{
			{
				in:   1136214245,
				out:  "1136214245000",
				in2:  1136214250,
				out2: "1136214250999",
			},
			{
				in:   1136214260,
				out:  "1136214260000",
				in2:  1136214256,
				out2: "1136214256999",
			},
			{
				in:   1136214266,
				out:  "1136214266000",
				in2:  1136214272,
				out2: "1136214272999",
			},
		}
		for _, v := range casesSuccess {
			t.Run("if get optimize period time  no err", func(t *testing.T) {
				res, res2 := GetOptimizePeriodTime(v.in, v.in2)
				require.Equal(t, v.out, res)
				require.Equal(t, v.out2, res2)
			})
		}
	})

	t.Run("TestGetOptimizePeriodTimeUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in, in2   int64
			out, out2 string
		}{
			{
				in:   1136214245,
				out:  "1136214245",
				in2:  1136214250,
				out2: "1136214250",
			},
			{
				in:   1136214245,
				out:  "1136214245999",
				in2:  1136214250,
				out2: "1136214250000",
			},
			{
				in:   1136214245,
				out:  "",
				in2:  1136214250,
				out2: "",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if get optimize period time  no equal no err", func(t *testing.T) {
				res, res2 := GetOptimizePeriodTime(v.in, v.in2)
				require.NotEqual(t, v.out, res)
				require.NotEqual(t, v.out2, res2)
			})
		}
	})
}

func TestCheckTimeFormatByLen(t *testing.T) {
	t.Run("TestCheckTimeFormatByLenSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in, in2, out string
		}{
			{
				in:  "02.01.2006 15:04:05 UTC",
				in2: "02.01.2006 15:06:10 UTC",
				out: "date",
			},
			{
				in:  "1600004533640",
				in2: "1600004536740",
				out: "unix",
			},
			{
				in:  " ",
				in2: " ",
				out: "",
			},
			{
				in:  "",
				in2: "",
				out: "",
			},
		}
		for _, v := range casesSuccess {
			t.Run("if check time format by len  no err", func(t *testing.T) {
				res := CheckTimeFormatByLen(v.in, v.in2)
				require.Equal(t, v.out, res)
			})
		}
	})

	t.Run("TestCheckTimeFormatByLenUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in, in2, out string
		}{
			{
				in:  "02.01.2006 15:04:05 UTC",
				in2: "02.01.2006 15:06:10 UTC",
				out: "unix",
			},
			{
				in:  "1600004533640",
				in2: "1600004536740",
				out: "date",
			},
			{
				in:  " ",
				in2: " ",
				out: "unix",
			},
			{
				in:  "",
				in2: "",
				out: "test",
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if check time format by len  no equal no err", func(t *testing.T) {
				res := CheckTimeFormatByLen(v.in, v.in2)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}
