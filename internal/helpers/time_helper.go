package helpers

import (
	"strconv"
	"time"
)

const (
	FormatDate     = "02.01.2006 15:04:05 UTC"
	FormatDateUnix = "1600004533640"
)

func GetFormatTime(s string) (int64, error) {
	t, err := time.Parse(FormatDate, s)
	if err != nil {
		return 0, err
	}
	return t.Unix(), nil
}

func GetUnixTimeNow() int64 {
	return time.Now().UTC().Unix()
}

func GetUnixMillisecondTimeNow() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

func GetOptimizePeriodTime(from, till int64) (string, string) {
	f := strconv.FormatInt(from*int64(time.Microsecond), 10)
	t := strconv.FormatInt((till+1)*int64(time.Microsecond)-1, 10)

	return f, t
}

func CheckTimeFormatByLen(from, till string) string {
	format := ""
	if len(from) == len(FormatDate) && len(till) == len(FormatDate) {
		format = "date"
	}
	if len(from) == len(FormatDateUnix) && len(till) == len(FormatDateUnix) {
		format = "unix"
	}
	return format
}
