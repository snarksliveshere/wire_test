package configs

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

type DBConfig struct {
	DBPath string `envconfig:"DB_PATH" default:"./data/db/"`
}

func GetDBConfig() DBConfig {
	var conf DBConfig
	err := envconfig.Process("grpc_server", &conf)
	if err != nil {
		log.Fatalf("%s, %s", "failed to init config", err)
	}
	return conf
}
