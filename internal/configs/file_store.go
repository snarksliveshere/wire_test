package configs

import (
	"log"

	"github.com/kelseyhightower/envconfig"
)

type FileConfig struct {
	PathToFile    string `envconfig:"PATH_TO_FILE" default:"./data/parquet/"`
	FileExtension string `envconfig:"FILE_EXTENSION" default:".parquet"`
}

func GetFileConfig() FileConfig {
	var conf FileConfig
	err := envconfig.Process("", &conf)
	if err != nil {
		log.Fatalf("%s, %s", "failed to init config", err)
	}
	return conf
}
