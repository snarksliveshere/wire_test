package dto

import (
	"encoding/json"
)

type Event struct {
	Message  string `json:"message" parquet:"name=message, type=BYTE_ARRAY"`
	Hostname string `json:"hostname" parquet:"name=hostname, type=BYTE_ARRAY"`
	Program  string `json:"program" parquet:"name=program, type=BYTE_ARRAY"`
	Pid      int    `json:"pid" parquet:"name=pid, type=INT32"`
	Time     string `json:"time" parquet:"name=time, type=BYTE_ARRAY"`
}

func EncodeEvent(e Event) ([]byte, error) {
	data, err := json.Marshal(e)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func DecodeEvent(data string) (Event, error) {
	var e Event
	err := json.Unmarshal([]byte(data), &e)
	if err != nil {
		return e, err
	}
	return e, nil
}
