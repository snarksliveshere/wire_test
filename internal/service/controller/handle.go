package controller

import (
	"go.uber.org/zap"

	"gitlab.com/snarksliveshere/wire_test/internal/service/parquet"
	"gitlab.com/snarksliveshere/wire_test/internal/service/sysloghandle"
	"gitlab.com/snarksliveshere/wire_test/internal/store/db"
	"gitlab.com/snarksliveshere/wire_test/internal/store/file"
)

type Handle interface {
	GetDataFromDB(from, till string) ([]string, error)
	SaveToDB(s string) error
	SaveToFile(sl []string, from, till string) (string, error)
}

type HandleMsg struct {
	DB        db.SyslogDB
	FileStore file.SyslogFile
	slog      *zap.SugaredLogger
}

func (h HandleMsg) SaveToDB(s string) error {
	sl, err := sysloghandle.HandleMessage(s)
	if err != nil {
		return err
	}
	err = h.DB.SaveToDB(sl)
	if err != nil {
		return err
	}

	return nil
}

func (h HandleMsg) GetDataFromDB(from, till string) ([]string, error) {
	sl, err := h.DB.ReadFromDB(from, till)
	if err != nil {
		return nil, err
	}

	return sl, nil
}

func (h HandleMsg) SaveToFile(sl []string, from, till string) (string, error) {
	name := parquet.PrepareFileName(from, till)
	path, err := h.FileStore.SaveToFile(sl, name)
	if err != nil {
		return "", err
	}

	return path, nil
}

func NewHandleController(db db.SyslogDB, fileStore file.SyslogFile, slog *zap.SugaredLogger) Handle {
	return HandleMsg{
		DB:        db,
		FileStore: fileStore,
		slog:      slog,
	}
}
