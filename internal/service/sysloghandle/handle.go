package sysloghandle

import (
	"time"

	"gitlab.com/snarksliveshere/wire_test/internal/dto"
	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
	"gitlab.com/snarksliveshere/wire_test/internal/pkg/parse"
)

func HandleMessage(msg string) ([]byte, error) {
	ev := parse.NewEvent()
	parse.ParseMsg([]byte(msg), ev)
	t := time.Now().UTC().Format(helpers.FormatDate)
	jEvent, err := dto.EncodeEvent(dto.Event{
		Message:  ev.Message(),
		Hostname: ev.Hostname(),
		Program:  ev.Program(),
		Pid:      ev.Pid(),
		Time:     t,
	})
	if err != nil {
		return nil, err
	}

	return jEvent, nil
}
