package sysloghandle

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"

	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

func TestHandleMessage(t *testing.T) {
	t.Run("TestHandleMessageSuccess", func(t *testing.T) {
		casesSuccess := []struct {
			in  string
			out []byte
		}{
			{
				in:  `Nov  3 20:29:51 some xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some","program":"xdg-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
			{
				in:  `Nov  3 20:29:51 some_host xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some_host","program":"xdg-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
			{
				in:  `Nov  3 20:29:51 some_host xd-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some_host","program":"xd-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
		}
		for _, v := range casesSuccess {
			t.Run("if string parse  no err", func(t *testing.T) {
				res, err := HandleMessage(v.in)
				require.NoError(t, err)
				require.Equal(t, string(v.out), string(res))
			})
		}
	})

	t.Run("TestHandleMessageUnEqual", func(t *testing.T) {
		casesNotEqual := []struct {
			in  string
			out []byte
		}{
			{
				in:  `Nov  3 20:29:51 host1 xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some","program":"xdg-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
			{
				in:  `Nov  3 20:29:51 host2 xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some","program":"xdg-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
			{
				in:  `Nov  3 20:29:51 host3 xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`,
				out: []byte(`{"message":"Most likely you need to configure your SUID sandbox correctly","hostname":"some","program":"xdg-desktop-portal","pid":30500,"time":"` + time.Now().UTC().Format(helpers.FormatDate) + `"}`),
			},
		}
		for _, v := range casesNotEqual {
			t.Run("if string parse  no equal no err", func(t *testing.T) {
				res, err := HandleMessage(v.in)
				require.NoError(t, err)
				require.NotEqual(t, v.out, res)
			})
		}
	})
}
