package db

import (
	"bytes"
	"strconv"

	"github.com/dgraph-io/badger"

	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

type SyslogDB interface {
	SaveToDB(s []byte) error
	ReadFromDB(from, till string) ([]string, error)
}

func (b Badger) openDB() (*badger.DB, error) {
	db, err := badger.Open(badger.DefaultOptions(b.conn))
	if err != nil {
		return nil, err
	}
	return db, err
}

func (b Badger) SaveToDB(s []byte) error {
	db, err := b.openDB()
	if err != nil {
		return err
	}
	defer func() { _ = db.Close() }()

	timeKey := strconv.FormatInt(helpers.GetUnixMillisecondTimeNow(), 10)
	err = db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(timeKey), s)
		return err
	})
	if err != nil {
		return err
	}

	return nil
}

func (b Badger) ReadFromDB(from, till string) ([]string, error) {
	db, err := b.openDB()
	if err != nil {
		return nil, err
	}
	defer func() { _ = db.Close() }()

	var sl []string
	err = db.View(func(txn *badger.Txn) error {
		var opt badger.IteratorOptions
		itr := txn.NewIterator(opt)
		defer itr.Close()
		for itr.Seek([]byte(from)); itr.Valid(); itr.Next() {
			item := itr.Item()
			key := item.Key()
			if bytes.Compare(key, []byte(till)) > 0 {
				break
			}
			err := item.Value(func(val []byte) error {
				sl = append(sl, string(val))
				return nil
			})
			if err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return sl, nil
}
