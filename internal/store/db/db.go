package db

import (
	"context"

	"gitlab.com/snarksliveshere/wire_test/internal/configs"
)

type Badger struct {
	conn string
}

func InitDB(_ context.Context, cfg configs.DBConfig) (Badger, error) {
	b := Badger{
		conn: cfg.DBPath,
	}
	return b, nil
}
