package file

import (
	"context"

	"gitlab.com/snarksliveshere/wire_test/internal/configs"
)

type StoreFile struct {
	path      string
	extension string
}

func InitFileStore(_ context.Context, cfg configs.FileConfig) (StoreFile, error) {
	b := StoreFile{
		path:      cfg.PathToFile,
		extension: cfg.FileExtension,
	}
	return b, nil
}
