package file

import (
	"github.com/xitongsys/parquet-go-source/local"
	"github.com/xitongsys/parquet-go/writer"

	"gitlab.com/snarksliveshere/wire_test/internal/dto"
)

type SyslogFile interface {
	SaveToFile(sl []string, name string) (string, error)
}

func (f StoreFile) SaveToFile(sl []string, name string) (string, error) {
	filePath := f.path + name + f.extension
	fw, err := local.NewLocalFileWriter(filePath)
	if err != nil {
		return "", err
	}

	pw, err := writer.NewParquetWriter(fw, new(dto.Event), 4)
	if err != nil {
		return "", err
	}
	for _, v := range sl {
		e, err := dto.DecodeEvent(v)
		if err != nil {
			return "", err
		}
		if err = pw.Write(e); err != nil {
			return "", err
		}
	}
	if err = pw.WriteStop(); err != nil {
		return "", err
	}
	err = fw.Close()
	if err != nil {
		return "", err
	}

	return filePath, nil
}
