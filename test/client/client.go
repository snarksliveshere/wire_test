package clients

import (
	"context"
	"log"

	"google.golang.org/grpc"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/test/client/configs"
)

type Conn struct {
	GConn   *grpc.ClientConn
	Client  proto.SyslogClient
	Context context.Context
}

func Client(conf configs.AppConfig) *Conn {
	cc, err := grpc.Dial(conf.ListenIP+":"+conf.GRPCPort, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}

	return &Conn{
		GConn:   cc,
		Client:  proto.NewSyslogClient(cc),
		Context: context.Background(),
	}
}

//protoc api/proto/syslog.proto --go_out=plugins=grpc:.
