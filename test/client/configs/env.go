package configs

type Addr struct {
	GRPCPort string `envconfig:"GRPC_PORT" default:"6515" required:"true"`
	ListenIP string `envconfig:"LISTEN_IP" default:"0.0.0.0"`
}

type AppConfig struct {
	Addr
}
