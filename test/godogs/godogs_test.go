// +build bdd

package bdd

import (
	"errors"
	"flag"
	"os"
	"strconv"
	"testing"
	"time"

	"github.com/cucumber/godog"
	"github.com/cucumber/godog/colors"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

const (
	str     = `Nov  3 20:29:51 host xdg-desktop-portal[30500]: Most likely you need to configure your SUID sandbox correctly`
	fileDir = "../../"
)

var opts = godog.Options{Output: colors.Colored(os.Stdout)}

func init() {
	godog.BindFlags("godog.", flag.CommandLine, &opts)
}

func TestMain(m *testing.M) {
	flag.Parse()
	opts.Paths = flag.Args()

	status := godog.TestSuite{
		Name:                 "godogs",
		TestSuiteInitializer: InitializeTestSuite,
		ScenarioInitializer:  InitializeScenario,
		Options:              &opts,
	}.Run()

	if st := m.Run(); st > status {
		status = st
	}
	os.Exit(status)
}

func InitializeTestSuite(ctx *godog.TestSuiteContext) {
	ctx.AfterSuite(func() {
		defer func() { _ = bt.client.GConn.Close() }()
	})
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	//writelog
	ctx.Step(`^put (\d+) logs in db$`, putLogsInDb)
	ctx.Step(`^i get (\d+) logs from db$`, iGetLogsFromDb)
	//pause
	ctx.Step(`^i put (\d+) logs in db and pause after (\d+) logs$`, iPutLogsInDbAndPauseAfterLogs)
	//resume
	ctx.Step(`^i resume put (\d+) logs in db$`, iResumePutLogsInDb)
	//getperiodlog
	ctx.Step(`^check logs for the selected period$`, checkLogsForTheSelectedPeriod)
	ctx.Step(`^check file exist$`, checkFileExist)
}

func optimizeTime(n int64) string {
	t := time.Unix(n/int64(time.Microsecond), 0).UTC()
	str := t.Format(helpers.FormatDate)

	return str
}

func iGetLogsFromDb(num int) error {
	var sl *proto.LogResponse
	from := strconv.FormatInt(bt.from, 10)
	till := strconv.FormatInt(bt.till, 10)
	dates := &proto.DateRequest{From: from, Till: till}
	sl, err := bt.client.Client.GetLogData(bt.client.Context, dates)
	if err != nil {
		return err
	}
	if err != nil {
		return err
	}
	if len(sl.Data) != num {
		return errors.New("iGetLogsFromDb::not equal num")
	}

	return nil
}
