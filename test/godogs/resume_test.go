// +build bdd

package bdd

import (
	"strconv"
	"time"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/cmd/grpc"
	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

func iResumePutLogsInDb(num int) error {
	txt := &proto.StrRequest{Text: grpc.Resume}
	_, err := bt.client.Client.WriteLog(bt.client.Context, txt)
	if err != nil {
		return err
	}
	bt.from = helpers.GetUnixMillisecondTimeNow()
	for i := 0; i < num; i++ {
		s := str + " " + strconv.Itoa(i)
		txt := &proto.StrRequest{Text: s}
		_, err := bt.client.Client.WriteLog(bt.client.Context, txt)
		if err != nil {
			return err
		}
		time.Sleep(200 * time.Millisecond)
	}
	bt.till = helpers.GetUnixMillisecondTimeNow()

	return nil
}
