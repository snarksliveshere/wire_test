// +build bdd

package bdd

import (
	"log"

	clients "gitlab.com/snarksliveshere/wire_test/test/client"

	"github.com/kelseyhightower/envconfig"

	"gitlab.com/snarksliveshere/wire_test/test/client/configs"
)

var (
	conf configs.AppConfig
	bt   bddTest
)

type bddTest struct {
	client     *clients.Conn
	from, till int64
	filePath   string
}

func init() {
	err := envconfig.Process("", &conf)
	if err != nil {
		log.Fatalf("%s, %s", "failed to init config", err)
	}
	bt.client = clients.Client(conf)
}
