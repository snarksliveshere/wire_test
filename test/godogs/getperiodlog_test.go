// +build bdd

package bdd

import (
	"errors"
	"os"
	"path/filepath"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
)

func checkFileExist() error {
	if bt.filePath == "" {
		return errors.New("checkFileExist::file path is empty")
	}
	_, err := os.Stat(bt.filePath)
	if err != nil {
		return err
	}

	return nil
}

func checkLogsForTheSelectedPeriod() error {
	from := optimizeTime(bt.from)
	till := optimizeTime(bt.till)
	filePath, err := bt.client.Client.GetPeriodLog(bt.client.Context, &proto.DateRequest{From: from, Till: till})
	if err != nil {
		return err
	}
	if filePath.GetPath() == "" {
		return errors.New("checkLogsForTheSelectedPeriod::file path is empty")
	}
	bt.filePath = filepath.Join(fileDir, filePath.GetPath())

	return nil
}
