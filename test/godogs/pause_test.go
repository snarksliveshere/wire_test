// +build bdd

package bdd

import (
	"strconv"
	"time"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/cmd/grpc"
	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

func iPutLogsInDbAndPauseAfterLogs(arg1, arg2 int) error {
	bt.from = helpers.GetUnixMillisecondTimeNow()
	for i := 0; i < arg1; i++ {
		s := str + " " + strconv.Itoa(i)
		if i >= arg2 {
			s = grpc.Pause
		}
		txt := &proto.StrRequest{Text: s}
		_, err := bt.client.Client.WriteLog(bt.client.Context, txt)
		if err != nil {
			return err
		}
		time.Sleep(200 * time.Millisecond)
	}
	bt.till = helpers.GetUnixMillisecondTimeNow()

	return nil
}
