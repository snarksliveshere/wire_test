module gitlab.com/snarksliveshere/wire_test

go 1.14

require (
	github.com/cucumber/godog v0.10.0
	github.com/dgraph-io/badger v1.6.2
	github.com/dgraph-io/badger/v2 v2.2007.2 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/golangci/golangci-lint v1.23.8 // indirect
	github.com/google/wire v0.4.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/xitongsys/parquet-go v1.5.4
	github.com/xitongsys/parquet-go-source v0.0.0-20201108113611-f372b7d813be
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20201031054903-ff519b6c9102 // indirect
	golang.org/x/sys v0.0.0-20201101102859-da207088b7d1 // indirect
	golang.org/x/text v0.3.4 // indirect
	golang.org/x/tools v0.0.0-20200224181240-023911ca70b2
	google.golang.org/genproto v0.0.0-20201102152239-715cce707fb0 // indirect
	google.golang.org/grpc v1.33.1
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v2 v2.3.0 // indirect
)
