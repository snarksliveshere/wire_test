package grpc

import (
	"context"
	"errors"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/internal/helpers"
)

var status string

const (
	Pause   = "pause"
	Stopped = "stopped"
	Resume  = "resume"
	Working = "working"
)

func (s SyslogServer) PauseResume(_ context.Context, str *proto.StrRequest) error {
	if str.Text == Pause {
		status = Stopped
		return nil
	}
	if str.Text == Resume {
		status = Working
		return nil
	}
	if status == Stopped {
		return nil
	}
	err := s.grpcService.Handle.SaveToDB(str.Text)
	if err != nil {
		s.grpcService.slog.Error(err)
		return err
	}

	return nil
}

func (s SyslogServer) getTimeFormat(date *proto.DateRequest) (string, string, error) {
	if helpers.CheckTimeFormatByLen(date.From, date.Till) == "" {
		return "", "", errors.New("getPeriodLog::date can not be nil")
	}
	var from, till string
	if helpers.CheckTimeFormatByLen(date.From, date.Till) == "unix" {
		from = date.From
		till = date.Till
	}
	if helpers.CheckTimeFormatByLen(date.From, date.Till) == "date" {
		f, err := helpers.GetFormatTime(date.From)
		if err != nil {
			s.grpcService.slog.Error(err)
		}
		t, err := helpers.GetFormatTime(date.Till)
		if err != nil {
			s.grpcService.slog.Error(err)
		}
		from, till = helpers.GetOptimizePeriodTime(f, t)
	}
	return from, till, nil
}

func (s SyslogServer) GetLogData(_ context.Context, date *proto.DateRequest) (*proto.LogResponse, error) {
	if date == nil {
		return nil, errors.New("getPeriodLog::date can not be nil")
	}
	from, till, err := s.getTimeFormat(date)
	if err != nil {
		s.grpcService.slog.Error(err)
		return nil, err
	}
	sl, err := s.grpcService.Handle.GetDataFromDB(from, till)
	if err != nil {
		s.grpcService.slog.Error(err)
	}

	return &proto.LogResponse{Data: sl}, nil
}

func (s SyslogServer) WriteLog(ctx context.Context, str *proto.StrRequest) (*proto.Empty, error) {
	err := s.PauseResume(ctx, &proto.StrRequest{Text: str.Text})
	if err != nil {
		s.grpcService.slog.Error(err)
		return nil, err
	}

	return &proto.Empty{}, nil
}

func (s SyslogServer) GetPeriodLog(_ context.Context, date *proto.DateRequest) (*proto.FilePathResponse, error) {
	var answer string
	if date == nil {
		return nil, errors.New("getPeriodLog::date can not be nil")
	}
	f, err := helpers.GetFormatTime(date.From)
	if err != nil {
		s.grpcService.slog.Error(err)
	}
	t, err := helpers.GetFormatTime(date.Till)
	if err != nil {
		s.grpcService.slog.Error(err)
	}
	from, till := helpers.GetOptimizePeriodTime(f, t)
	sl, err := s.grpcService.Handle.GetDataFromDB(from, till)
	if err != nil {
		s.grpcService.slog.Error(err)
	}
	path, err := s.grpcService.Handle.SaveToFile(sl, from, till)
	if err != nil {
		s.grpcService.slog.Error(err)
	}
	if path == "" {
		answer = "Empty log by dates"
	}

	return &proto.FilePathResponse{Path: path, Answer: answer}, nil
}
