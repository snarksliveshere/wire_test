package grpc

import (
	"net"

	"google.golang.org/grpc"

	"gitlab.com/snarksliveshere/wire_test/api/proto"
	"gitlab.com/snarksliveshere/wire_test/internal/configs"
)

type SyslogServer struct {
	grpcService *ServiceGRPC
}

func Server(conf configs.AppConfig, grpcService *ServiceGRPC) {
	listenAddr := conf.ListenIP + ":" + conf.GRPCPort
	listen, err := net.Listen("tcp", listenAddr)
	if err != nil {
		grpcService.slog.Panic("failed to listen addr: %s, error: %v\n", listenAddr, err.Error())
	}
	grpcService.slog.Infof("Run GRPC server on: %s\n", listenAddr)
	grpcServer := grpc.NewServer()

	serv := SyslogServer{grpcService: grpcService}

	proto.RegisterSyslogServer(grpcServer, serv)
	err = grpcServer.Serve(listen)
	if err != nil {
		grpcService.slog.Panic(err.Error())
	}
}

//protoc api/proto/syslog.proto --go_out=plugins=grpc:.
