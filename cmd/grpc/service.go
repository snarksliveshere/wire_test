package grpc

import (
	"go.uber.org/zap"

	"gitlab.com/snarksliveshere/wire_test/internal/service/controller"
)

type ServiceGRPC struct {
	Handle controller.Handle
	slog   *zap.SugaredLogger
}

func NewGRPCService(handle controller.Handle, slog *zap.SugaredLogger) *ServiceGRPC {
	return &ServiceGRPC{
		Handle: handle,
		slog:   slog,
	}
}
